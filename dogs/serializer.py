from rest_framework import  serializers
from .models import DogsModel

class DogsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DogsModel
        fields = '__all__'

