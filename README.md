
<h1 align="center">Dogger_api</h1>

<p align="center">
  <a href="#dart-about">About</a> &#xa0; | &#xa0; 
  <a href="#rocket-technologies">Technologies</a> &#xa0; | &#xa0;
  <a href="#white_check_mark-requirements">Requirements</a> &#xa0; | &#xa0;
  <a href="#checkered_flag-starting">Starting</a> &#xa0; | &#xa0;
  <a href="#sparkles-features">Features</a> &#xa0; | &#xa0;
  <a href="#sparkles-desplieguevps">DespliegueVPS</a> &#xa0; | &#xa0;
  <a href="https://github.com/{{YOUR_GITHUB_USERNAME}}" target="_blank">Author</a>
</p>

<br>

## :dart: About ##

Se está desarrollando una plataforma llamada “Dogger”. El objetivo de la app es conectar “dueños” de perros con “paseadores” de perros. 
Diseñar e implementar el backend y frontend de las plataformas usando un API REST para la comunicación. 

## :sparkles: Links ##

- [Dogger-Front](https://dogger.digitaloak.co/)
- [Dogger-Api](https://api.digitaloak.co/dogger_api/)

## :rocket: Technologies ##

A continuacion se enumeran las diferentes tecnologias que se usaron:

- [Django](https://www.djangoproject.com/)
- [Django REST framework](https://www.django-rest-framework.org/)

## :white_check_mark: Requirements ##

Antes de iniciar :checkered_flag:, necesitaras [python](https://www.python.org/) y los siguientes modulos indalados:

```requirements
asgiref==3.4.1
certifi==2021.10.8
Django==3.2.9
django-cors-headers==3.10.0
django-filter==21.1
djangorestframework==3.12.4
djangorestframework-simplejwt==5.0.0
Markdown==3.3.4
PyJWT==2.3.0
pytz==2021.3
sqlparse==0.4.2
wincertstore==0.2
```

A api fue creada con un entorno virtual de Anaconda y en windows, por lo que puede encontrar conflictos al instalarlo en un entorno virtual diferente como virtualenv, en caso de que tenga errores al instalar un modulo en especifico, eliminelo del requirements.txt

## :checkered_flag: Starting ##

Luego de bajar el repo, vamos a lanzar la aplicacion en modo desarrollo, lo primero es ir al archivo ubicado en `dogger_api/settings.py` y cambiamos el valor de la variable `DEBUG` al valor `True`:

```python
DEBUG = False 
```

Al hacer esto el proyecto ya estara listo para empezar nuevamente el desarrollo, ahora veremos como iniciar la apicacion:

```bash
# Clona el proyecto
$ git clone https://gitlab.com/iandres.montes/dogger_api

# Accede a la carpeta del proyecto
$ cd dogger_api

# Instala las dependencias
$ pip install -r requirements.txt

# Corre el proyecto
$ manage.py runserver
#o 
$ python manage.py runserver 


# El servidor se inicializara en el puerto 8000 <http://localhost:8000>
```

## :sparkles: DespliegueVPS ##
A continuacion, explicare como se realizo el despliegue del servicio en el servidor VPS que esta ubicado en canada y de acceso ssh.

Lo primero fue preparar el entorno, se utilizo [Anaconda](https://www.anaconda.com/) y fuera del proceso de instalacion se siguieron los siguientes pasos

```bash
#se creo un nuevo entorno
$ conda create -n graviti python=3.8

#luego de eso se activa el entorno creado
$ conda activate graviti

```

teniendo el entorno listo y suponiendo que ya estan los repositorios clonados y los requirements.txt instalados, procedemos a crear el demonio del servicio con Systemctl

para eso vamos a crear un nuevo archivo en la siguiente ruta:

```bash
$ nano /etc/systemd/system/dogger_api.service
```
y vamos a pegar el siguiente contenido:

```conf
[Unit]
Description=gunicorn daemon for dogger_api
After=network.target

[Service]
User=root
Group=www-data
WorkingDirectory=/home/ubuntu/delete-me/dogger_api
ExecStart=/root/anaconda3/envs/graviti/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind localhost:8000 dogger_api.wsgi:application

[Install]
WantedBy=multi-user.target
```
Guardamos: `CTRL + X`, `Y`, `ENTER`

En `WorkingDirectory` se indica la ruta donde esta el proyecto y en `ExecStart` se indica el comando a ejecutar.
Luego de que hayamos guardado ese archivo podemos reiniciar el demonio ejecutando los siguientes comandos:

```bash
# Reiniciamos el demonio
$ systemctl daemon-reload

# Habilitamos el demonio para que inicie con el kernel del sistema
$ systemctl enable <daemon-name>

# Iniciamos el demonio
$ systemctl start <daemon-name>
```

Luego de eso, continuamos con la configuracion de nginx, para esto utilizamos un subdominio de `dogger.digitaloak.co`:
a continuacion la configuracion de nginx para el front
```conf
server {
  listen 80;
  listen [::]:80;

  root /var/www/html/dogger/;
  index index.html index.htm index.nginx-debian.html;

  server_name dogger.digitaloak.co;

  location / {
          try_files $uri /index.html;
  }
}
```
ahora la configuracion de nginx para el backend:

```conf
server {
    listen 80;
    listen [::]:80;

    server_name api.digitaloak.co;
    
    ...

    location /dogger_api/ {
            proxy_pass http://localhost:8000/;
    }

}
```


## :sparkles: Features ##
Para poder iniciar el desarrollo hubieron varias curvas de aprendizaje, tecnologias y lenguajes que no conocia y no habia utilizado antes, en el caso de la api fue django, por lo que dedique la mayor parte del tiempo en buscar informacion y practica para poder iniciar el desarrollo de la api como tal, el diseño de la api se realizo pensando en tres perfiles diferentes, guest (clientes), walker (paseadores) y dogs (perros), al tenerlos asi, como perfiles diferentes, realizar las relaciones en bases de datos y en el patron de diseño seria mucho mas sencillo.

a continuacion, dejare la documentacion de cada endpoint correspondiente a la entrega minima del proyecto para esta fecha:

:heavy_check_mark: Registrar usuarios "dueños". `/guest/api/register` \
Esta parte fue complicada, representa el inicio de la curva de aprendizaje, ya que se inicio pensando en los metodos de autenticacion, asi que pense en implementar algo sencillo para tener un Beta que entregar y conforme pasara el tiempo, podria ir añadiendo mas mejoras al codigo y la funcionalidad.\

El registro de los clientes se realiza por medio de una clase en la app "guest" del proyecto, Api para registro en la base de datos de guest, el cuerpo del request es:
``` json
{
    name:(str),
    lastname:(str),
    email:(str),
    password:(str),
    mobile:(str) 
}

```
Este motodo espera el cuerpo del request por medio de un request con metodo post para poder procesar y validar la informacion del usuario y asi registrarlo de manera correcta.

:heavy_check_mark: Registrar usuarios "paseadores". `/walker/api/register`\

Este es un caso como el anterior, se duplico el metodo utilizado en la app de 'guest'.
Api para registro en la base de datos de walker, el cuerpo del request es:

```json
{
    name:(str),
    lastname:(str),
    email:(str),
    password:(str),
    mobile:(str) 
}
```
Este motodo espera el cuerpo del request por medio de un request con metodo post para poder procesar y validar la informacion del usuario y asi registrarlo de manera correcta.

:heavy_check_mark: Un dueño puede registrar perros con su tamaño (Grande, Chico, Mediano). `/dogs/api/create`\
Para lograr eso, luego del registro y el login, tendria la oportunidad de poder crear los perfiles de su perrito, en este caso en la app de dogs en el proyecto se implemento esta clase para poder hacer el resgitro de los perritos:

```
class DogsCreateApi(generics.CreateAPIView):
    queryset = DogsModel.objects.all()
    serializer_class = DogsSerializer
```
se utilizo un serializador y una de las formas que tiene django de poder crear un CRUD de forma mucho mas sencilla, en este caso espera el metodo POST con el envio de los datos correspodientes, excluyendo "id" y "created_at"

```json
{
    "name": "",
    "size": "",
    "guest_id": "",
    "walker": ""
}
```

:heavy_check_mark: Un dueño puede reservar a un paseador en específico. `/dogs/api/create`\

En este caso se utilizaron los tres modelos para crear esta relacion y en lugar de crear una tabla nueva, se llego a la conclusion de que el perro solo podia tener un paseador, por lo que tener una columna en la DB podria ser la mejor opcion, se realizo una columna nueva en la tabla de dogs.\

Actualizacion o vista de un perro, se reciben parametro de diferentes formas, pero solo tiene dos metodos disponibles PUT y GET

```python
dog = DogsModel.objects.get(id = id)
...
dog.walker = data.get('walker')
dog.save()
return Response({"error":False, "response":data})
```

:heavy_check_mark: Un paseador puede tener un máximo de 3 perros al mismo tiempo. `PUT`:`/dogs/api/byID/<int:id>`\

Para este caso se realizo un filtrado de los perritos que ya tuvieran el mismo paseador y si habia mas de tres, entonces, se retornaria un response diferente para que el front haga la respectiva toma de decisiones.

```python
def put(self, request, id):
    dog = DogsModel.objects.get(id = id)
    data = json.loads(request.body)
    if dog:
        count_dogs = DogsModel.objects.filter(walker=data.get('walker')).all().count()
        if count_dogs < 3:
            dog.walker = data.get('walker')
            dog.save()
            return Response({"error":False, "response":data})
        else:
            return Response({"error":True, "response":{"message":"Este paseador ya tiene muchos perros asignados"}})
    else:
        return Response({"error":True, "response":None})
```

:heavy_check_mark: Desde el punto de vista del paseador, puede recibir perros de múltiples dueños en cada reserva.\
Esto fue sencillo, no requiere ningun tipo de validacion, y el contador de perritos con el limite 3, siempre marcaria cuando ya fuesen suficientes sin importar el dueño

:x: Un paseador puede definir horarios para pasear ciertos tamaños de perro (chico, mediano, grande o alguna combinación de estos).\
:x: El dueño/paseador debe recibir una notificación al ocurrir algun evento con el paseo/perro\

Hecho con el :heart:! por <a href="https://gitlab.com/iandres.montes" target="_blank">iandres.montes</a>

&#xa0;

<a href="#top">Volver al top</a>
