from django.urls import path
from .api import Register, Login, GuestDeleteApi, GuestUpdateApi
urlpatterns = [
    path('api/login',Login.as_view()),
    path('api/register',Register.as_view()),
    path('api/<int:pk>',GuestUpdateApi.as_view()),
    path('api/<int:pk>/delete',GuestDeleteApi.as_view()),
] 