from django.db import models

class DogsModel(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    name = models.TextField()
    size = models.TextField()
    guest_id = models.TextField(null=True)
    walker = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now=True)
