from rest_framework import  serializers
from .models import WalkerModel

class WalkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalkerModel
        fields = ['id', 'name', 'lastname', 'email', 'mobile', 'created_at']

class WalkerSerializerAuth(serializers.ModelSerializer):
    class Meta:
        model = WalkerModel
        fields = ['id', 'name', 'lastname', 'email', 'password', 'mobile', 'created_at']