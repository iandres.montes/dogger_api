from django.db import models

class GuestModel(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    name = models.TextField()
    lastname = models.TextField()
    email = models.TextField()
    password = models.TextField()
    mobile = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now=True)