from rest_framework import generics
from .serializer import DogsSerializer
from .models import DogsModel
from rest_framework.views import APIView
import json
from rest_framework.response import Response
from _helper.helper import Helper
from walker.models import WalkerModel

class DogsListApi(generics.ListAPIView):
    queryset = DogsModel.objects.all()
    serializer_class = DogsSerializer

class DogsCreateApi(generics.CreateAPIView):
    queryset = DogsModel.objects.all()
    serializer_class = DogsSerializer

class DogsUpdateApi(generics.RetrieveUpdateAPIView):
    queryset = DogsModel.objects.all()
    serializer_class = DogsSerializer

class DogsDeleteApi(generics.DestroyAPIView):
    queryset = DogsModel.objects.all()
    serializer_class = DogsSerializer

class GetByID(APIView):
    """ 
    Actualizacion o vista de un perro, se reciben parametro de diferentes formas, pero solo tiene dos metodos disponibles PUT y GET
    """
    def __init__(self) -> None:
        self.helper = Helper()
    
    def put(self, request, id):
        dogs = DogsModel.objects.get(id = id)
        data = json.loads(request.body)
        if dogs:
            count_dogs = DogsModel.objects.filter(walker=data.get('walker')).all().count()
            if count_dogs < 3:
                dogs.name = data.get('name')
                dogs.size = data.get('size')
                dogs.walker = data.get('walker')
                dogs.save()
                return Response({"error":False, "response":data})
            else:
                return Response({"error":True, "response":{"message":"Este paseador ya tiene muchos perros asignados"}})

        else:
            return Response({"error":True, "response":None})

    def get(self, request, id):
        """
        Este metodo se utiliza para retornar la informacion de un perro
        """
        dogs = DogsModel.objects.get(id = id)
        if dogs:
            data = dogs.__dict__
            del data['_state']
            return Response({"error":False, "response":data})
        else:
            return Response({"error":True, "response":None})

class GetListByGuest(APIView):
    """
    Enpoint para obtener una lista de los perros filtrada por id de usuario y tambien retorna la informacion del paseador
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def getWalker(self, item):
        walker = WalkerModel.objects.get(id = item['walker'])
        if walker:
            walker = walker.__dict__
            del walker['_state']
            del walker['password']
            item['walker'] = walker
        return item

    def get(self, request, id):
        """
        Este metodo se utiliza para devolver una lista de perros filtrada por el id del usuario
        """
        dogs = DogsModel.objects.filter(guest_id__exact = id).all().values('id', 'name', 'size', 'walker', 'created_at')
        if dogs:
            dogs_list = [
                self.getWalker(x) if x['walker'] else x for x in dogs
            ]
            return Response({"error":False, "response":dogs_list})
        else:
            return Response({"error":False, "response":[]})

