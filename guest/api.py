from rest_framework import generics
from .serializer import GuestSerializer, GuestSerializerAuth
from .models import GuestModel
from rest_framework.views import APIView
import json
from rest_framework.response import Response
from _helper.helper import Helper

class GuestUpdateApi(generics.RetrieveUpdateAPIView):
    """Actualizar informacion de guest"""
    queryset = GuestModel.objects.all()
    serializer_class = GuestSerializer

class GuestDeleteApi(generics.DestroyAPIView):
    """Eliminar perfil de guest"""
    queryset = GuestModel.objects.all()
    serializer_class = GuestSerializer

class Login(APIView):
    """
    Endpoint para la validacion de usuario atravez de usuario y contraseña, el cuerpo JSON del request es el siguiente:
    {
        email:(str),
        password:(str)
    }

    -> JSON 
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def post(self, request):
        """
        Este metodo se utiliza para validar la cuenta de usuario atraves de correo y contraseña, valida correo, existencia de usuario, validacion de contraseña.
        """
        data = json.loads(request.body)
        try:
            email = data['email']
            password = data['password']
        except Exception as err:
            return Response({"error":True, "response":{"message":"Tiene que enviar todos los datos"}})

        if self.helper.validate_email(email):
            user = GuestModel.objects.filter(email=email).first()
            if user:
                if self.helper.validate_password(password, user.password):
                    return  Response({"error":False, "response":{"login":True, "id":user.id,"message":"Login correcto"}})
                else:
                    return  Response({"error":True, "response":{"login":False, "message":"Login incorrecto"}})
            else:
                return Response({"error":True, "response":{"message":"Aun no hay registro con ese correo electronico"}})
                
        else:
            return Response({"error":True, "response":{"message":"Tiene que enviar un email valido para inciar sesion"}})

class Register(APIView):
    """
    Api para registro en la base de datos de guest, el cuerpo del request es:

    {
        name:(str),
        lastname:(str),
        email:(str),
        password:(str),
        mobile:(str) 
    }

    -> JSON
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def post(self, request):
        """
        Este motodo espera el cuerpo del request por medio de un request con metodo post para poder procesar y validar la informacion del usuario y asi registrarlo de manera correcta.
        """
        data = json.loads(request.body)
        try:
            name = data['name']
            lastname = data['lastname']
            email = data['email']
            password = data['password']
            mobile = data['mobile']
        except Exception as err:
            return Response({"error":True, "response":{"message":"Tiene que enviar todos los datos"}})

        if self.helper.validate_email(email):
            validate = self.helper.validate_email_exist(email, GuestModel)
            if not validate:
                password_hash = self.helper.create_password(password) 
                new_guest = GuestModel(
                    name=name,
                    lastname=lastname, 
                    email=email, 
                    password=password_hash,
                    mobile=mobile
                )
                new_guest.save()
                return  Response({"error":False, "response":{"register":True, "id":new_guest.id, "message":"usuario creado satisfactoriamente"}})
            else:
                return Response({"error":True, "response":{"message":"Ya hay un usuario registrado con ese correo"}})