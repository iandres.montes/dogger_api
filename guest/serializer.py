from rest_framework import  serializers
from .models import GuestModel

class GuestSerializer(serializers.ModelSerializer):
    class Meta:
        model = GuestModel
        fields = ['id', 'name', 'lastname', 'email', 'mobile', 'created_at']


class GuestSerializerAuth(serializers.ModelSerializer):
    class Meta:
        model = GuestModel
        fields = ['id', 'name', 'lastname', 'email', 'password', 'mobile', 'created_at']