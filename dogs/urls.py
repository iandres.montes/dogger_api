from django.urls import path
from .api import DogsCreateApi, DogsDeleteApi, GetListByGuest, GetByID
urlpatterns = [
    path('api/byID/<int:id>', GetByID.as_view()),
    path('api/byGuest/<int:id>', GetListByGuest.as_view()),
    path('api/create',DogsCreateApi.as_view()),
    path('api/<int:pk>/delete',DogsDeleteApi.as_view()),
] 