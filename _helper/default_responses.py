import json
from collections import namedtuple
from json import JSONEncoder

responses_dict = {
    "register":{
        "email_exist":{"error":True, "message":"El email ya esta registrado por otro usuario"},
        "email_exist":{"error":True, "message":"El email ya esta registrado por otro usuario"}
    }
}

def toObjt(item):
    return namedtuple('Responses', item.keys())(*item.values())

responses = json.loads(json.dumps(responses_dict), object_hook=toObjt)

