from django.urls import path
from .api import Register, WalkerDeleteApi, WalkerListApi, WalkerUpdateApi, Login, GetByID

urlpatterns = [
    path('api/register',Register.as_view()),
    path('api/login', Login.as_view()),
    path('api', WalkerListApi.as_view()),
    path('api/byID/<int:id>', GetByID.as_view()),
    path('api/<int:pk>/update',WalkerUpdateApi.as_view()),
    path('api/<int:pk>/delete',WalkerDeleteApi.as_view()),
]  