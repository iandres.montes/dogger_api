from rest_framework import generics
from .serializer import WalkerSerializer, WalkerSerializerAuth
from .models import WalkerModel
from rest_framework.views import APIView
from .models import WalkerModel
import json
from rest_framework.response import Response
from _helper.helper import Helper

class WalkerListApi(generics.ListAPIView):
    """ Endpoint creado para poder retornar una lista completa 
    de los walkers registrados en la base de datos *sin contraseña* """
    queryset = WalkerModel.objects.all()
    serializer_class = WalkerSerializer

class WalkerUpdateApi(generics.RetrieveUpdateAPIView):
    """
        Endpoint creado para actualizar la informacion de los walkers mediante un metodo PUT 
        /api/<int:pk>/update 
    """
    queryset = WalkerModel.objects.all()
    serializer_class = WalkerSerializer

class WalkerDeleteApi(generics.DestroyAPIView):
    """
        Eliminar usuario mediante un metodo DELETE con el identificador del usuario <int:pk>
        /api/<int:pk>/delete
    """
    queryset = WalkerModel.objects.all()
    serializer_class = WalkerSerializer

class Login(APIView):
    """
    Endpoint para la validacion de usuario atravez de usuario y contraseña, el cuerpo JSON del request es el siguiente:
    {
        email:(str),
        password:(str)
    }

    -> JSON 
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def post(self, request):
        """
        Este metodo se utiliza para validar la cuenta de usuario atraves de correo y contraseña, valida correo, existencia de usuario, validacion de contraseña.
        """
        data = json.loads(request.body)
        try:
            email = data['email']
            password = data['password']
        except Exception as err:
            return Response({"error":True, "response":{"message":"Tiene que enviar todos los datos"}})

        if self.helper.validate_email(email):
            user = WalkerModel.objects.filter(email=email).first()
            if user:
                if self.helper.validate_password(password, user.password):
                    return  Response({"error":False, "response":{"login":True, "message":"Login correcto", "id":user.id}})
                else:
                    return  Response({"error":True, "response":{"login":False, "message":"Login incorrecto"}})
            else:
                return Response({"error":True, "response":{"message":"Aun no hay registro con ese correo electronico"}})
                
        else:
            return Response({"error":True, "response":{"message":"Tiene que enviar un email valido para inciar sesion"}})

class Register(APIView):
    """
    Api para registro en la base de datos de walker, el cuerpo del request es:

    {
        name:(str),
        lastname:(str),
        email:(str),
        password:(str),
        mobile:(str) 
    }

    -> JSON
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def post(self, request):
        """
        Este motodo espera el cuerpo del request por medio de un request con metodo post para poder procesar y validar la informacion del usuario y asi registrarlo de manera correcta.
        """
        data = json.loads(request.body)
        try:
            name = data['name']
            lastname = data['lastname']
            email = data['email']
            password = data['password']
            mobile = data['mobile']
        except Exception as err:
            print(err)
            return Response({"error":True, "response":{"message":"Tiene que enviar todos los datos"}})

        try:
            if self.helper.validate_email(email):
                validate = self.helper.validate_email_exist(email, WalkerModel)
                if not validate:
                    password_hash = self.helper.create_password(password) 
                    new_walker = WalkerModel(
                        name=name,
                        lastname=lastname, 
                        email=email, 
                        password=password_hash,
                        mobile=mobile
                    )
                    new_walker.save()
                    return  Response({"error":False, "response":{"register":True, "message":"usuario creado satisfactoriamente", "id":new_walker.id}})
                else:
                    return Response({"error":True, "response":{"message":"Ya hay un usuario registrado con ese correo"}})
            else:
                return Response({"error":True, "response":{"message":"Por favor, digite un correo valido"}})
        except Exception as err:
            print(err)


class GetByID(APIView):
    """
    Enpoint para asignacion de Walker a Dogs mediante un id enviado por post 
    {
        email:(str),
        password:(str)
    }

    -> JSON 
    """
    def __init__(self) -> None:
        self.helper = Helper()

    def get(self, request, id):
        """
        Este metodo se utiliza para validar la informacion del walker y asignarle un nuevo dog.
        """
        walker = WalkerModel.objects.get(id = id)
        if walker:
            data = walker.__dict__
            del data['_state']
            return Response({"error":False, "response":data})
        else:
            return Response({"error":True, "response":None})