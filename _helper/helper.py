import re
from django.contrib.auth.hashers import make_password, check_password

class Helper:
    """Metodo creado para poder separa el codigo indispensable y probablemente inestable fuera de los controladores, para poder buggear los errores de manera mas rapida y concreta.
    """
    def validate_password(self, password:str, encode_pass):
        """Validacion de contraseña para el incio de sesion
        
        Keyword arguments:
        password -- contraseña en str y utf8
        encode_pass -- contraseña guardada en base de datos y codificada anteriormente, la codificacion se realiza normalmente en el resitro de usuario.
        Return: Boolean.
        """
        
        return check_password(password, encode_pass)

    def create_password(self, password:str) -> None:
        """Creacion del hash de la contraseña encriptada para luego ser guardad en base de datos.
        
        Keyword arguments:
        password -- contraseña en str y utf8
        Return: STR | None 
        """
        
        return make_password(password)

    def validate_email_exist(self, email:str, model:object):
        """Validar existencia del correo electronico en la base de datos
        
        Keyword arguments:
        email -- correo del usuario str y utf8
        model -- modelo:object del que se piensa filtrar el email.
        Return: Bool | None
        """
        
        try:
            user = model.objects.filter(email=email).first()
            if user:
                return True
            else:
                return False
        except Exception as err:
            print(err)
            return False

    def validate_email(self, email:str):
        """En este punto se evalua que el correo este escrito correctamente.
        
        Keyword arguments:
        email -- correo del usuario str y utf8
        Return: Bool | None
        """
        
        return True if(re.search('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$',email)) else False
        
        